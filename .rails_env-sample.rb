# DEVELOPER STUFF
ENV['DEVELOPER_NAME'] = "WHAT YOUR NAME"
ENV['DEVELOPER_PHONE'] = "2121112222"
ENV['DEVELOPER_EMAIL'] = "developer@bandsite.com"
ENV['DEVELOPER_PASS'] = "BandSite123!"

ENV['DEVELOPMENT_URL'] = "http://localhost:3000"

# Other URLs that this software can't derive
ENV['STAGING_URL'] = "https://api-staging.bandsite.com"

#  Like where the frontend app is coming from
ENV['FRONTEND_URL'] = "https://bandsite.com"

ENV['POSTGRES_HOST'] = "postgresql"
ENV['POSTGRES_PORT'] = "5432"
ENV['POSTGRES_POOL'] = "5"
ENV['POSTGRES_USERNAME'] = "postgres"
ENV['POSTGRES_PASSWORD'] = "postgres"

# The name of this app in its long and polite form.
ENV['PUBLIC_APP_NAME'] = "The Band Site"

# Sending text/sms through TWILIO.

# Your Twilio Account
ENV['TWILIO_SID']  = "A7eee8f00f7df80e0e74da33d0dfc"
ENV['TWILIO_AUTH'] = "db98193640f2063601a5202b118"
ENV['TWILIO_NUM']  = "+1 888 888 8888"

# Sending Email through SendGrid.
ENV['SENDGRID_API_KEY'] = "SG.RlYpf0K1QGCyUDFJOUFKJKJdk3wxZjb6mGKVifXKHA44"

# Sending Email through Google.
# ENV['SMTP_DOMAIN'] = "somedomain.com"
# ENV['SMTP_SERVER'] = "smtp.gmail.com"
# ENV['SMTP_PORT'] = "587"
# ENV['SMTP_USERNAME'] = "notifier@somedomain.com"
# ENV['SMTP_PASSWORD'] = "IloveFOSS2!"

# Sending Email through Amazon SES.
# ENV['SMTP_SERVER'] = "email-smtp.us-west-2.amazonaws.com"
# ENV['SMTP_PORT'] = "465"
# ENV['SMTP_TLS'] = "Yes"
# ENV['SMTP_DOMAIN'] = "somedomain.com"
# ENV['SMTP_SES_USER'] = "ses-smtp-user.20200221-202053"
# ENV['SMTP_USERNAME'] = "AKIAEF6HWJ2Y55KFVUID"
# ENV['SMTP_PASSWORD'] = "BCHMJlgobbeldyGRBjzcDWJqp6g0tYJIGXJDienzp5UB"

# Sending push notifications through Firebase.
ENV['FCM_PRODUCTION_KEY'] = "AAuWsswmQdanK1j12Cbuwx6o-OFIBQkwxrIAYQOEWaE5pSBH_ssaP77xDoWAjh9BQtkoUka5jPaz686Ng4HSodzn91eZJ4g8JpoxjneLoYdkF987YQow13HwE2"
ENV['FCM_DEVELOPMENT_KEY'] = "AK73ZZoEYOofqWOFHpZRhju_fKiviQiePLnF_6Q90MTTVlMQZghOQaccz7Wig4zlMnfXatGCNtosPL4vdqI1v3XsOMNsWqay3ot4FNrCnJnxSE6b5Bttb0JXD8o"

# Storing things in AWS.
ENV['AWS_KEY'] = ""
ENV['AWS_SECRET'] = ""
ENV['AWS_BUCKET'] = "somedomain-data"
ENV['AWS_REGION'] = "us-west-2"

# Hubspot services
# ENV['HUBSPOT_API_KEY'] = 'aaa-bbb-12a23421-2355-1d1c-8190-n642x1jgr6f'
