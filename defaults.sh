# defaults.sh: -*- Shell-script -*-  DESCRIPTIVE TEXT.
# 
# EVIXP - Generic API Server Backend for use in health studies.
# Copyright (C) 2019 The EVIXP Authors (See AUTHORS)
# This software is licensed under the GNU Affero General Public License, Version 3.
# Please see the file 'LICENSE' for more detailed information.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
#  Author: Brian J. Fox (bfox@opuslogica.com)
#  Birthdate: Tue Nov 26 06:57:38 2019.
dir=$(dirname $self)
self=${BASH_SOURCE[0]}

if [[ -r ${dir}/.env ]]; then
    source ${fdir}/.env
elif [[ -r ${dir}/../../shared/THE.env ]]; then
    source ${dir}/../../shared/THE.env
fi

if [[ "$dir" == *"api-staging"* ]]; then
    appname=markcent_staging
else
    appname=${appname:-markcent}
fi

database=postgresql
app_dbuser="${APP_DBUSER:-markcent_user}"
app_dbpass="${APP_DBPASS:-markcent_pass}"
app_dbhost="${app_dbhost:-${1:-${RDS_ADDRESS:-localhost}}}"
